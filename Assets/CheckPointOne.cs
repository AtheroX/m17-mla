﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPointOne : MonoBehaviour{

	CheckPoints cps;

	public void OnTriggerEnter2D(Collider2D collision) {
		if(collision.TryGetComponent<GottaGoFast>(out GottaGoFast gottaGoFast)) {
			cps.ThroughCheck(this);
		}
	}

	public void Set(CheckPoints cps) {
		this.cps = cps;
	}
}
