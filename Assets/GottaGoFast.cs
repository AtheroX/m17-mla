﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.MLAgents;
using Unity.MLAgents.Actuators;
using Unity.MLAgents.Sensors;

public class GottaGoFast : Agent{

	Rigidbody2D rb;
	Vector3 init;
	public float speed,turnSpeed;
	float direction;
    public float duration = 5f;

	public CheckPoints cps;

	int intento;

	public void Start() {
		rb = GetComponent<Rigidbody2D>();
		init = transform.localPosition;

		cps.onPlayerPassCheckPoint += OnPlayerGood;
		cps.onPlayerWrongsCheckPoint += OnPlayerBad;
		cps.onPlayerEndsCheckPoints += OnPlayerEnd;
	}

	public override void OnEpisodeBegin() {
		AddReward(-1f);
		intento++;
		transform.localPosition = init;
		rb.rotation = 0;
		rb.velocity = Vector2.zero;
		if (duration != 0)
			StartCoroutine(Coso(intento));

		cps.Reset();
	}

    public IEnumerator Coso(int i) {
		yield return new WaitForSecondsRealtime(duration);
		if (i != intento)
			yield return null;
		else {
			AddReward(-3);
			EndEpisode();
			yield return null;
		}
    }

    public override void CollectObservations(VectorSensor sensor) {
		CheckPointOne cp = cps.GetNext();
		float dirToNext = Vector3.Dot(transform.forward, cp.transform.forward);
        sensor.AddObservation(dirToNext);
        sensor.AddObservation(rb.velocity);
    }

    public override void OnActionReceived(ActionBuffers actions) {
		float fw = actions.DiscreteActions[0];
		float turn = actions.DiscreteActions[1];
        //Debug.Log(fw + " a " + turn);

        Run(fw, turn);
	}
    
	public override void Heuristic(in ActionBuffers actionsOut) {
		ActionSegment<int> ca = actionsOut.DiscreteActions;
		ca[0] = (int)Input.GetAxisRaw("Vertical")+1;
		ca[1] = (int)Input.GetAxisRaw("Horizontal")+1;

		//Debug.Log(ca[0]+" a "+ ca[1]);
		
	}

	public void Run(float fw, float turn) {
		turn--;
        fw--;
		//Debug.Log(fw + " " + turn);

		rb.AddForce(transform.up * fw * Time.deltaTime * speed);
        //direction = Mathf.Sign(Vector2.Dot(rb.velocity, transform.up));
        rb.rotation -= turn*2f*turnSpeed* Time.deltaTime;// * rb.velocity.magnitude * direction;
	}

	public void OnTriggerEnter2D(Collider2D collision) {
		if (collision.CompareTag("Wall")) {
			AddReward(-.5f);
			EndEpisode();
		}
	}

	private void OnPlayerGood(object sender, System.EventArgs e) {
		AddReward(.05f);
	}

	private void OnPlayerBad(object sender, System.EventArgs e) {
		AddReward(-.5f);
	}

	private void OnPlayerEnd(object sender, System.EventArgs e) {
		AddReward(.5f);
		EndEpisode();
	}
}
