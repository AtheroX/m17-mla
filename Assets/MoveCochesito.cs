﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveCochesito : MonoBehaviour{
    Rigidbody2D rb;
    public float speed;
    float direction;

    void Start(){
        rb = GetComponent<Rigidbody2D>();

    }

    void Update(){
        float fw = Input.GetAxis("Vertical");
        float turn = -Input.GetAxis("Horizontal");
        Run(fw, turn);

    }

    public void Run(float fw, float turn) {

        Debug.Log(fw + " " + turn);

        rb.AddForce(transform.up * fw * Time.deltaTime * speed);
        //direction = Mathf.Sign(Vector2.Dot(rb.velocity, transform.up));
        rb.rotation += turn;
    }
}
