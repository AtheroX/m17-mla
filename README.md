**Carreritas brum brum - Unity MLAgents**
_M17_
---

![Circuito](./img/carretera.png)

### Introducción
---

Hacer estos coches ha sido bastante prueba y error, sólo hay que ver todos los intentos que he hecho. 

Algunos modelos han intentado hacer trampas, otros no aprendían, otros se estrellaban por la velocidad.

![Todos los modelos creados](./img/Todos.png)

He sacado en claro que más entrenamiento no siempre va a mejor, porque en la propia configuración del 
entrenamiento hay un valor llamado _Learning Rate_ que hace que contra más tiempo pase menos aprende.

En mi caso justamente aplicó lo suficiente en un cúmulo de errores generando que desaprendiese las cosas buenas
que hacía y aprendiese malos comportamientos

![Fail de tiempo](./img/MásNoMejor.png)


### Mi entrenamiento
---

Mi entrenamiento ha sido programar un coche el cual es capaz de moverse adelante y atrás y girar a la izquierda y a la derecha.
Por esto he decidido hacer un entrenamiento discreto con 2 vectores de 3, el primero elige si avanzar, quedarse quieto o retroceder.
El otro es igual pero para los giros.

En cuanto a las observaciones, tiene un raycast y 3 valores más, el producto escalar entre el forward del siguiente checkpoint y su forward y su velocidad.

![Behaviour en el Inspector](./img/Behaviour.png)

```csharp
CheckPointOne cp = cps.GetNext();
float dirToNext = Vector3.Dot(transform.forward, cp.transform.forward);
sensor.AddObservation(dirToNext);   //Un float   - un  valor
sensor.AddObservation(rb.velocity); //Un Vector2 - dos valores
``` 

A parte de esas observaciones también tiene un raycast sensor  el cual mira adelante y un poco a los lados y puede detectar paredes y CheckPoints

![Behaviour en el Inspector](./img/ray.png)

Después el script donde está configurado el agente únicamente contiene la velocidad del coche, la velocidad de giro, la duración (puedes poner una cantidad de segundos y tras eso acaba un episodio) y por último es gestor de CheckPoints.

![Script en el Inspector](./img/script.png)


### Modelos

He seguido 3 modelos de entrenamiento. 

El primero es que a partir de estas observaciones hiciese elecciones aleatorias y aprendiese en base a eso, es decir por refuerzo. Ha sido un modelo bastante lento pero obtuvo buenos resultados, apenas se chocaba.

El segundo tenía unos archivos de grabado que hice usando uno de los componentes de la librería de ML llamado Demostration Recorder. Para usarlo tuve que poner el modo de Heurística y jugar un rato, eso generó archivos de "demo" que le pasé al modelo a partir de su archivo de configuración. Dio resultados de forma rápida pero de vez en cuando hacía cosas raras y no llegó a mucho.

![Componente de grabación](./img/gravasao.png)

El último modelo ha sido el mejor sin duda, este parte de el modelo con mejor resultado por refuerzo, y le hice 2 iteraciones, es decir, partía de la base de hacerlo bien pero seguía haciendo modificaciones grandes y de esta forma mejoraba cosas como su velocidad, por ejemplo en tomar las curvas.

![Diferencias en los archivos de configuración](./img/diferencias.png)



![Video de demostración](./img/dejavu.mp4)


